import express from 'express';
import bodyParser from 'body-parser';

import newsRoutes from './routes/news.js'

const app = express();
const PORT = 5000;

app.use(bodyParser.json());

app.use('/news', newsRoutes);


app.listen(PORT, () => console.log(`Server running on port http://localhost:${PORT}`))