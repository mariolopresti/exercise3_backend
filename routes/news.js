import express from "express";
import * as fs from "fs";

const router = express.Router();

// get all news ( max 25 )
router.get('/', (req, res) => {
	const data = fs.readFileSync('./news.json');
	const newsList = JSON.parse(data);

  const n  = Math.floor(Math.random() * newsList.length);
  const k  = Math.floor(Math.random() * newsList.length);
  let numbElement = n + k < newsList.length ? n+k : newsList.length;
	numbElement = numbElement === n + k ?  numbElement + 1 : numbElement;
	let newArray = newsList.slice(n, numbElement);

	newArray.map((element) => {
		element.body = element.body.substr(0,100)
		return	element
	})

	res.send(newArray);
})

// get news with id
router.get('/:id', (req ,res) => {
	let { id } = req.params;

	const data = fs.readFileSync('./news.json');
	const newsList = JSON.parse(data);

	if(	 id === 'random'){
		id = Math.floor(Math.random() * newsList.length);
	}
	const newsSearched = newsList.find((news) => news.id === id.toString());
	res.send(newsSearched);
});

export default router;